import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders}  from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
readonly APIUrl="https://localhost:5009/gateway"; //https://localhost:5009/api/tipocambio
readonly PhotoUrl = "http://localhost:58013/Photos/";

readonly tokenJWT ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3VzZXJkYXRhIjoiQFBQMDEiLCJuYmYiOjE2Nzc3MjYyODIsImV4cCI6MTY3NzczMzQ4MiwiaWF0IjoxNjc3NzI2MjgyfQ.tLYbk52eULsHGmeaRv5goR8VTkQi39pDXJ7pgJerq9g";
  constructor(private http:HttpClient) { }

  getDepList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/department');
  }

  addDepartment(val:any){
    return this.http.post(this.APIUrl+'/Department',val);
  }

  updateDepartment(val:any){
    return this.http.put(this.APIUrl+'/Department',val);
  }

  deleteDepartment(val:any){
    return this.http.delete(this.APIUrl+'/Department/'+val);
  }


  getEmpList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Employee');
  }

  addEmployee(val:any){
    return this.http.post(this.APIUrl+'/Employee',val);
  }

  updateEmployee(val:any){
    return this.http.put(this.APIUrl+'/Employee',val);
  }

  deleteEmployee(val:any){
    return this.http.delete(this.APIUrl+'/Employee/'+val);
  }


  UploadPhoto(val:any){
    return this.http.post(this.APIUrl+'/Employee/SaveFile',val);
  }

  getAllDepartmentNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/Employee/GetAllDepartmentNames');
  }

  // BEGIN Usuari
  getSEUsuarioList():Observable<any[]>{

    let headers = new HttpHeaders().set('Authorization', 'Bearer ' +  this.tokenJWT); 
    return this.http.get<any>(this.APIUrl+'/consultar-listamoneda', { headers }); //seusuario this.APIUrl+

  }
  



  getSEUsuario(){
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' +  this.tokenJWT); 
    return this.http.get<any>(this.APIUrl+'/consultar-tipocambio',{ headers }); //seusuario this.APIUrl+
  
  }



  addSEUsuario(val:any){
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' +  this.tokenJWT); 
  return this.http.post(this.APIUrl+'/insertar',val,{ headers }); //seusuario
  }


  updateSEUsuario(val:any){
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' +  this.tokenJWT); 
    return this.http.put(this.APIUrl+'/update',val, { headers }); //seusuario
    console.log(val);
  }

  deleteSEUsuario(val:any){
    return this.http.delete(this.APIUrl+'/eliminar/'+val);
  }
  // END Usuario






    // BEGIN Entidad
    getBAEntidadList():Observable<any[]>{
     // return this.http.get<any>(this.APIUrl+'/baentidad');

     let headers = new HttpHeaders().set('Authorization', 'Bearer ' +  this.tokenJWT); 
     return this.http.get<any>(this.APIUrl+'/consultar-tipocambio?monto=1&monedaOrigen=PEN&monedaDestino=JPY', { headers }); //seusuario this.APIUrl+
     console.log();
    }
  
    addBAEntidad(val:any){
      return this.http.post(this.APIUrl+'/baentidad',val);
    }
  
    updateBAEntidad(val:any){
      return this.http.put(this.APIUrl+'/baentidad',val);
    }
  
    deleteBAEntidad(val:any){
      return this.http.delete(this.APIUrl+'/baentidad/'+val);
    }
    // END Entidad

}
