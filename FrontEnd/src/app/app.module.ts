import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DepartmentComponent } from './department/department.component';
import { ShowDepComponent } from './department/show-dep/show-dep.component';
import { AddEditDepComponent } from './department/add-edit-dep/add-edit-dep.component';

import { EmployeeComponent } from './employee/employee.component';
import { ShowEmpComponent } from './employee/show-emp/show-emp.component';
import { AddEditEmpComponent } from './employee/add-edit-emp/add-edit-emp.component';

import { SEUsuarioComponent } from './seusuario/seusuario.component';
import { ShowUsuComponent } from './seusuario/show-usu/show-usu.component';
import { AddEditUsuComponent } from './seusuario/add-edit-usu/add-edit-usu.component';

import { BAEntidadComponent } from './baentidad/baentidad.component';
import { ShowEntComponent } from './baentidad/show-ent/show-ent.component';
import { AddEditEntComponent } from './baentidad/add-edit-ent/add-edit-ent.component';

import{SharedService} from './services/shared.service';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    DepartmentComponent,
    ShowDepComponent,
    AddEditDepComponent,
    
    EmployeeComponent,
    ShowEmpComponent,
    AddEditEmpComponent,

    SEUsuarioComponent,
    ShowUsuComponent,
    AddEditUsuComponent,

    BAEntidadComponent,
    ShowEntComponent,
    AddEditEntComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
