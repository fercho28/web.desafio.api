import { Component, OnInit,Input } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-ent',
  templateUrl: './add-edit-ent.component.html',
  styleUrls: ['./add-edit-ent.component.css']
})
export class AddEditEntComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() dep:any;
  EntidadId:number;
  EntidadName:string;
  UsuarioName:string;
  
  UsuariosList:any=[];

  ngOnInit(): void {
    this.loadDepartmentList();
  }

  loadDepartmentList(){
    this.service.getSEUsuarioList().subscribe((data:any)=>{
      this.UsuariosList=data;

      this.EntidadId=this.dep.EntidadId;
      this.EntidadName=this.dep.EntidadName;
      this.UsuarioName=this.dep.UsuarioName;
    });
  }

  addEntidad(){
    var val = {EntidadId:this.EntidadId,
      EntidadName:this.EntidadName,
      UsuarioName:this.UsuarioName };

    this.service.addBAEntidad(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateEntidad(){
    var val = {EntidadId:this.EntidadId,
      EntidadName:this.EntidadName,
      UsuarioName:this.UsuarioName};

    this.service.updateBAEntidad(val).subscribe(res=>{
    alert(res.toString());
    });
  }

}

