import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-ent',
  templateUrl: './show-ent.component.html',
  styleUrls: ['./show-ent.component.css']
})
export class ShowEntComponent implements OnInit {

  constructor(private service:SharedService) { }

  EmployeeList:any=[];

  ModalTitle:string;
  ActivateAddEditEmpComp:boolean=false;
  emp:any;

  EntidadIdFilter:string="";
  EntidadNameFilter:string="";
  UsuarioNameFilter:string="";
  EntidadListWithoutFilter:any=[];

  ngOnInit(): void {
    this.refreshEmpList();
  }

  addClick(){
    this.emp={
      EntidadId:0,
      EntidadName:"",
      UsuarioName:"",
    }
    this.ModalTitle="Add Entidad";
    this.ActivateAddEditEmpComp=true;

  }

  editClick(item){
    console.log(item);
    this.emp=item;
    this.ModalTitle="Edit Entidad";
    this.ActivateAddEditEmpComp=true;
  }

  deleteClick(item){
    if(confirm('Estás seguro??')){
      this.service.deleteBAEntidad(item.EntidadId).subscribe(data=>{
        alert(data.toString());
        this.refreshEmpList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditEmpComp=false;
    this.refreshEmpList();
  }


  refreshEmpList(){
    this.service.getBAEntidadList().subscribe(data=>{
      this.EmployeeList=data;
      this.EntidadListWithoutFilter=data;
      console.log(data);
    });
    
  }

  FilterFn(){
    var EntidadIdFilter = this.EntidadIdFilter;
    var EntidadNameFilter = this.EntidadNameFilter;
    var UsuarioNameFilter = this.UsuarioNameFilter;

    this.EmployeeList = this.EntidadListWithoutFilter.filter(function (el){
        return el.EntidadId.toString().toLowerCase().includes(
          EntidadIdFilter.toString().trim().toLowerCase()
        )&&
        el.EntidadName.toString().toLowerCase().includes(
          EntidadNameFilter.toString().trim().toLowerCase()
        )&&
        el.UsuarioName.toString().toLowerCase().includes(
          UsuarioNameFilter.toString().trim().toLowerCase()
        )
    });
  }


}

