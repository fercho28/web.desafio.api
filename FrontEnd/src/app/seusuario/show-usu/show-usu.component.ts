import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-usu',
  templateUrl: './show-usu.component.html',
  styleUrls: ['./show-usu.component.css']
})
export class ShowUsuComponent implements OnInit {

  constructor(private service:SharedService) { }

  UsuarioList:any=[];

  ModalTitle:string;
  ActivateAddEditDepComp:boolean=false;
  dep:any;


  monedaFilter:string="";
  tipoCambioUSDFilter:string="";
  monedaListWithoutFilter:any=[];

  ngOnInit(): void {
    this.refreshSEUsuarioList();
  }

  addClick(){
    this.dep={

      //moneda:"",
      //tipoCambioUSD:""

      moneda:"",
      tipoCambioUSD:""
     // monto:"" //agregue

      
    }


    this.ModalTitle="Add Moneda";
    this.ActivateAddEditDepComp=true;
  }

  editClick(item){
    this.dep=item;
    this.ModalTitle="Edit Moneda";
    this.ActivateAddEditDepComp=true;
  }

  deleteClick(item){
    if(confirm('Estás seguro??')){
      this.service.deleteSEUsuario(item.UsuarioId).subscribe(data=>{
        alert(data.toString());
        this.refreshSEUsuarioList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditDepComp=false;
    this.refreshSEUsuarioList();
  }


  refreshSEUsuarioList(){
    this.service.getSEUsuarioList().subscribe(data=>{
      //this.UsuarioList=data;
      //this.UsuarioListWithoutFilter=data;
        this.UsuarioList=data;
      this.monedaListWithoutFilter=data;
      console.log(data);
    });
  }

  FilterFn(){
    var UsuarioIdFilter = this.monedaFilter;
    var UsuarioNameFilter = this.tipoCambioUSDFilter;

    this.UsuarioList = this.monedaListWithoutFilter.filter(function (el){
        return el.UsuarioId.toString().toLowerCase().includes(
          UsuarioIdFilter.toString().trim().toLowerCase()
        )&&
        el.UsuarioName.toString().toLowerCase().includes(
          UsuarioNameFilter.toString().trim().toLowerCase()
        )
    });
  }

  sortResult(prop,asc){
    this.UsuarioList = this.monedaListWithoutFilter.sort(function(a,b){
      if(asc){
          return (a[prop]>b[prop])?1 : ((a[prop]<b[prop]) ?-1 :0);
      }else{
        return (b[prop]>a[prop])?1 : ((b[prop]<a[prop]) ?-1 :0);
      }
    })
  }

}
